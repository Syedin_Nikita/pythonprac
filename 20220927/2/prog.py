a = list(eval(input()))
exe = a.copy() 
l = len(a)
for i in range(l):
    exe[i]=(exe[i]**2 % 100)

for i in range(l):
    for j in range(l-i-1):
        if exe[j] > exe[j+1]:
            a[j], a[j+1] = a[j+1], a[j]
            exe[j], exe[j+1] = exe[j+1], exe[j]
print(a)