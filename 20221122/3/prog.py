import struct, sys


file = sys.stdin.buffer.read()
try:
    if struct.unpack("4s", file[8:12])[0] == b"WAVE":
        print("Size={}, Type={}, Channels={}, Rate={}, Bits={}, Data size={}".format(
            struct.unpack("i", file[4:8])[0],
            struct.unpack("h", file[20:22])[0],
            struct.unpack("h", file[22:24])[0],
            struct.unpack("i", file[24:28])[0],
            struct.unpack("h", file[34:36])[0],
            struct.unpack("i", file[40:44])[0]
            ))
    else:
        print("NO")
except:
    print("NO")
