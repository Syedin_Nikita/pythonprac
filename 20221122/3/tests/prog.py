import struct, wave, sys


file = sys.stdin.buffer.read()
print(wave.open(file).getnchannels())
try:
    if struct.unpack("4s", file[8:12])[0] == b"WAVE" and (wave.open(file).getnchannels()==1 or wave.open(file).getnchannels()==2):
        print("Size={}, Type={}, Channels={}, Rate={}, Bits={}, Data size={}".format(
            struct.unpack("i", file[4:8])[0],
            struct.unpack("h", file[20:22])[0],
            struct.unpack("h", file[22:24])[0],
            struct.unpack("i", file[24:28])[0],
            struct.unpack("h", file[34:36])[0],
            struct.unpack("i", file[40:44])[0]
            ))
    else:
        print("NO")
except:
    print("NO")
