import asyncio, random, sys
from copy import copy

async def merge(a, b, start, mid, finish, event_in1, event_in2, event_out):
    await asyncio.gather(event_in1.wait(), event_in2.wait())
    a = copy(a)
    point1, point2 = start, mid
    for j in range(start, finish):
        if point2 >= finish or point1 < mid and a[point1] < a[point2]:
            b[j] = a[point1]
            point1 = point1+1
        else:
            b[j] = a[point2]
            point2 = point2+1

    event_out.set()


async def mtasks(a):
    save = copy(a)

    def CORE(event_out, l, r):
        if r <= l+1:
            event_out.set()
            return []
        event_in1, event_in2 = asyncio.Event(), asyncio.Event()
        coroutine = merge(save, save, l, round((l + r) / 2), r, event_in1, event_in2, event_out)
        return CORE(event_in1, l, round((l + r) / 2)) + CORE(event_in2, round((l + r) / 2), r) + [coroutine]

    return CORE(asyncio.Event(), 0, len(a)), save

exec(sys.stdin.read())

