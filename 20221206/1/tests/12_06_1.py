import asyncio
from collections import deque

que=asyncio.Queue()
stack=asyncio.LifoQueue()

flag=0
async def writer (que, delay):
    c = 0
    while flag==0:
        await asyncio.sleep (delay)
        await que.put(str(c)+"_"+str(delay))
        c+=1

async def stacker(stack, que):
    while flag==0:
        await stack.put(await que.get())

async def reader (stack, quantity, delay):
    SelfCounter = 0
    while SelfCounter!=quantity:
        await asyncio.sleep (delay)
        print(await stack.get())
        SelfCounter += 1
    flag=1

async def main():
    Hub = list(eval(input()))
    await asyncio.gather(writer(que, Hub[0]), writer(que, Hub[1]), stacker(stack, que), reader(stack, Hub[-1], Hub[2]))
asyncio.run(main())