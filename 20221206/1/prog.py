import asyncio
from collections import deque

global_event = asyncio.Event()


async def writer(peremen1, peremen2):
    count = 0
    await asyncio.sleep(peremen2)
    while not global_event.is_set():
        await peremen1.put(f"{count}_{peremen2}")
        await asyncio.sleep(peremen2)
        count += 1
    await peremen1.put(None)


async def stacker(peremen1, peremen3):
    while not global_event.is_set():
        locperem = await peremen1.get()
        await peremen3.put(locperem)


async def reader(peremen3, peremen10, peremen2):
    await asyncio.sleep(peremen2)
    for i in range(peremen10):
        locperem = await peremen3.get()
        print(locperem)
        await asyncio.sleep(peremen2)
    global_event.set()


async def main():
    sod1, sod2, sod3, c = map(int, eval(input()))
    queue = asyncio.Queue()
    stack = asyncio.LifoQueue()
    await asyncio.gather(
        writer(queue, sod1),
        writer(queue, sod2),
        stacker(queue, stack),
        reader(stack, c, sod3),
    )


asyncio.run(main())
