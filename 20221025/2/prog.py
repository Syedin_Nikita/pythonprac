import itertools, sys
def slide(a, num):
	store=len(a)
	hub=itertools.tee(a, store)
	c=0
	for i in hub:
		try:
			yield from itertools.islice(i, c, c+num)
			c+=1
		except:
			yield from itertools.islice(i, c)
			c+=1

exec(sys.stdin.read())