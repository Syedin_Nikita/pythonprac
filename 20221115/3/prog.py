class Alpha:
    __slots__ = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

    def __init__(self, **kwargs):
        for i in kwargs.keys():
            setattr(self, i, kwargs[i])
    
    def __str__(self):
        buf = [f"{i}: {getattr(self, i)}" for i in self.__slots__ if getattr(self, i, 0)]
        return ", ".join(buf)


class AlphaQ:
    myalphas = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

    def __init__(self, **kwargs):
        for i in kwargs.keys():
            if i in AlphaQ.myalphas:
                self.__dict__[i] = kwargs[i]
            else:
                raise AttributeError
    
    def __setattr__(self, nm, val):
        if nm in AlphaQ.myalphas:
            self.__dict__[nm] = val
        else:
            raise AttributeError

    def __getattr__(self, nm):
        if nm in AlphaQ.myalphas:
            if getattr(self.__dict__, nm, 0):
                return self.__dict__[nm]
        else:
            raise AttributeError

    def __str__(self):
        buf = [f"{i}: {self.__dict__[i]}" for i in sorted(self.__dict__.keys())]
        return ", ".join(buf)


import sys
exec(sys.stdin.read())
