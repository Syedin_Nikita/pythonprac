def objcount(cls):
    cls.counter = 0
    flag = -1
    realinit = cls.__init__
    if hasattr(cls.__dict__, '__del__'):
        realdel = cls.__del__
        flag = 1

    def decinit(self, *args):
        cls.counter += 1
        realinit(self, *args)

    def decdel(self):
        cls.counter -= 1
        if flag > 0:
            realdel(self)

    cls.__init__ = decinit
    cls.__del__ = decdel
    return cls

import sys
exec(sys.stdin.read())
