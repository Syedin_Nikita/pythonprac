from collections import UserString
from math import floor

class DivStr(UserString):
    def __init__(self, arg=''):
        super().__init__(arg)
        self.leng = len(arg)

    def __floordiv__(self, n):
        step = floor(self.leng/n)   
        for i in range(n):
            yield self[i*step:(i+1)*step]

    def __mod__(self, n):
        step = self.leng - self.leng % n
        return self[step:]

import sys
exec(sys.stdin.read())
