from math import sqrt

class InvalidInput(Exception): pass
class BadTriangle(Exception): pass


def koefline(xx, yy):
    return sqrt(sum(x**2 for x in [xx,yy]))


def triangleSquare(inStr):
    try:
        (x1, y1), (x2, y2), (x3, y3) = eval(inStr)
    except:
        raise InvalidInput

    for i in range(1,4):
        xtest, ytest = eval(f"x{i}, y{i}")
        if type(xtest) != int or type(ytest) != int:
            raise InvalidInput

    k1 = koefline(x1-x2, y1-y2)
    k2 = koefline(x2 - x3, y2 - y3)
    k3 = koefline(x1 - x3, y1 - y3)
    if k1 < k2 + k3 and k3 < k2 + k1 and k2 < k1 + k3 and all((k1, k2, k3)):
        p = (k1 + k2 + k3) / 2
        s = sqrt(p * (p-k1) * (p-k2) * (p-k3))
        if round(s)!=0:
            return s 
        else:
            raise BadTriangle
    else:
        raise BadTriangle


def catcher(fobj):
    try:
        tri = triangleSquare(fobj)
    except InvalidInput:
        print("Invalid input")
    except BadTriangle:
        print("Not a triangle")
    else:
        print('%.2f' % tri)


try:
    while s :=input():
        catcher(s)
except EOFError:
    pass
