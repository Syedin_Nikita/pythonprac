import inspect
import sys
def decorator_function_2(name, func):
    def wrapper(*args, **kwargs):
        print(f"{name}: {args[1:]}, {kwargs}")
        return func(*args, **kwargs)
    return wrapper



class dump(type):
    def __new__(cls, name, future_class_parents, future_class_attr, **future_ndigits):
        for i,j in future_class_attr.items():
            if inspect.isfunction(j):
                future_class_attr[i]=decorator_function_2(i, j)
        return type.__new__(cls, name, future_class_parents, future_class_attr)


exec(sys.stdin.read())
