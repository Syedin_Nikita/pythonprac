import inspect
import types
import sys

class check(type):
    def __new__(cls, name, future_class_parents, future_class_attr, **future_ndigits):
        def check_annotations(self):
            for name, AT in inspect.get_annotations(self.__class__).items():
                try:
                    save=getattr(self, name)
                except:
                    return False
                if inspect.isfunction(save):
                    pass
                else:
                    sort=type(save)
                    if type(AT) == types.GenericAlias:
                        AT = AT.__origin__
                    if sort != AT and not issubclass(sort, AT):
                        return False
            return True
        future_class_attr["check_annotations"]=check_annotations
        return type.__new__(cls, name, future_class_parents, future_class_attr)


exec(sys.stdin.read())