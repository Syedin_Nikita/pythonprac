from collections import Counter
inp=input().lower()
res=[]
for i in range(1, len(inp)):
    if inp[i-1]+inp[i] in res or inp[i].isalpha()==False or inp[i-1].isalpha()==False:
        pass
    else:
        res.append(inp[i-1]+inp[i])
print(len(res))
