from fractions import Fraction

def Fractions(args):
    s, w, fst_deg = args[0], args[1], args[2]
    fst_coefs = [args[i + 3] for i in range(fst_deg + 1)]
    scn_deg = args[fst_deg + len(fst_coefs)]
    scn_coefs = args[-(fst_deg + 1):]
    numerator = sum([fst_coefs[-i] * s**(i-1) for i in range(1, fst_deg + 2)])
    denumerator = sum([scn_coefs[-i] * s**(i-1) for i in range(1, scn_deg + 2)])
    if denumerator != 0:
        return Fraction(numerator / denumerator) == Fraction(w)
    else: 
        return False

arr=eval(input())
print(Fractions(arr))
