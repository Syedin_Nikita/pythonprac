import math
def recfun(num, a):
	l=len(a)
	if l==1 and a[0]!=num:
		return False
	if l%2==0:
		b=a[l//2-1]
		if b>num:
			return recfun(num, a[:l//2-1])
		elif b<num:
			return recfun(num, a[l//2:])
		else:
			return True
	else:
		b=a[math.floor(l//2)]
		if b>num:
			return recfun(num, a[:math.floor(l//2)])
		elif b<num:
			return recfun(num, a[math.floor(l//2)+1:])
		else:
			return True

print(recfun(*eval(input())))