def function(x, y):
	if isinstance(x, list): 
		return [i for i in x if i not in y]
	elif isinstance(x, tuple):
		return tuple([i for i in x if i not in y])
	else:
		return x-y

a=eval(input())
x, y=a[0], a[1]
print(function(x, y))
