def ch(N1):
    sum=0
    N1Save=N1
    while N1Save>0:
        sum+=N1Save%10
        N1Save=N1Save//10
    if sum==6:
        return ':=)'
    else:
        return(N1)

N=int(input())
M1=ch(N*N)
M2=ch(N*(N+1))
M3=ch(N*(N+2))
print(f"{N} * {N} = {M1} {N} * {N+1} = {M2} {N} * {N+2} = {M3}")
M1=ch(N*(N+1))
M2=ch((N+1)*(N+1))
M3=ch((N+1)*(N+2))
print(f"{N+1} * {N} = {M1} {N+1} * {N+1} = {M2} {N+1} * {N+2} = {M3}")
M1=ch((N+2)*N)
M2=ch((N+2)*(N+1))
M3=ch((N+2)*(N+2))
print(f"{N+2} * {N} = {M1} {N+2} * {N+1} = {M2} {N+2} * {N+2} = {M3}")

