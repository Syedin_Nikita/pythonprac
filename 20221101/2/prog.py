class Triangle:
    
    def __init__(self, a, b, c):
        self.p1 = a
        self.p2 = b
        self.p3 = c

    def __abs__(self):
        from math import hypot
        a = hypot(self.p1[0] - self.p2[0], self.p1[1] - self.p2[1])
        b = hypot(self.p2[0] - self.p3[0], self.p2[1] - self.p3[1])
        c = hypot(self.p1[0] - self.p3[0], self.p1[1] - self.p3[1])
        if a + b > c and b + c > a and a + c > b:
            p = (a + b + c) / 2
            S = (p * (p - a) * (p - b) * (p - c))**0.5
        else:
            S = 0
        return round(S, 5)

    def __bool__(self):
        return abs(self) != 0
    
    def __lt__(self, oth):
        if isinstance(oth, Triangle):
            return abs(self) < abs(oth)

    def __contains__(self, oth):
        if isinstance(oth, Triangle):
            if abs(oth) == 0 or abs(oth) == 0 and abs(self) == 0:
                return True
            if abs(oth) <= abs(self):
                res = []
                for i in range(1,4):
                    xt, yt = eval(f'oth.p{i}[0], oth.p{i}[1]')
                    l1 = (self.p1[0] - xt) * (self.p2[1] - self.p1[1]) - (self.p2[0] - self.p1[0]) * (self.p1[1] - yt)
                    l2 = (self.p2[0] - xt) * (self.p3[1] - self.p2[1]) - (self.p3[0] - self.p2[0]) * (self.p2[1] - yt)
                    l3 = (self.p3[0] - xt) * (self.p1[1] - self.p3[1]) - (self.p1[0] - self.p3[0]) * (self.p3[1] - yt)
                    res.append((l1 > 0 and l2 > 0 and l3 > 0) or (l1 < 0 and l2 < 0 and l3 < 0))
                return all(res)
            return False
          
    def __and__(self, oth):
        if isinstance(oth, Triangle):
            if bool(self) and bool(oth):
                res1 = []
                for i in range(1,4):
                    xt, yt = eval(f'oth.p{i}[0], oth.p{i}[1]')
                    l1 = (self.p1[0] - xt) * (self.p2[1] - self.p1[1]) - (self.p2[0] - self.p1[0]) * (self.p1[1] - yt)
                    l2 = (self.p2[0] - xt) * (self.p3[1] - self.p2[1]) - (self.p3[0] - self.p2[0]) * (self.p2[1] - yt)
                    l3 = (self.p3[0] - xt) * (self.p1[1] - self.p3[1]) - (self.p1[0] - self.p3[0]) * (self.p3[1] - yt)
                    res1.append((l1 >= 0 and l2 >= 0 and l3 >= 0) or (l1 <= 0 and l2 <= 0 and l3 <= 0))
                
                res2 = []
                for i in range(1,4):
                    xt, yt = eval(f'self.p{i}[0], self.p{i}[1]')
                    l1 = (oth.p1[0] - xt) * (oth.p2[1] - oth.p1[1]) - (oth.p2[0] - oth.p1[0]) * (oth.p1[1] - yt)
                    l2 = (oth.p2[0] - xt) * (oth.p3[1] - oth.p2[1]) - (oth.p3[0] - oth.p2[0]) * (oth.p2[1] - yt)
                    l3 = (oth.p3[0] - xt) * (oth.p1[1] - oth.p3[1]) - (oth.p1[0] - oth.p3[0]) * (oth.p3[1] - yt)
                    res2.append((l1 >= 0 and l2 >= 0 and l3 >= 0) or (l1 <= 0 and l2 <= 0 and l3 <= 0))
                return any(res1+res2)
            else:
                return False

import sys
exec(sys.stdin.read())
