class Grange():
    def __init__(self, b0, q, bmax) -> None:
        self.b0 = b0
        self.q = q
        self.bmax = bmax
    
    def __str__(self) -> str:
        return f"grange({self.b0}, {self.q}, {self.bmax})"
    
    def __repr__(self) -> str:
        return str(self)

    def __iter__(self):
        tmp = self.b0
        while tmp < self.bmax:
            yield tmp
            tmp *=self.q
    
    def __len__(self):
        count = 0
        for i in self:
            count +=1
        return count

    def __getitem__(self, val):
        if type(val) != slice:
            return self.b0*self.q**val
        else:
            b0, bmax, degr = val.start, val.stop, val.step
            if degr is None:
                return Grange(b0, self.q, bmax)
            else:
                return Grange(b0, self.q**degr, bmax)

import sys
exec(sys.stdin.read())
