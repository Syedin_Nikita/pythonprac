class Omnibus:
    examples = {}
    ownexamples = {}

    def __init__(self, *args):
        pass

    def __setattr__(self, name, val):
        if Omnibus.examples.get(name, 0):
            Omnibus.examples[name] += 1
        else:
            Omnibus.examples[name] = 1
        if self.ownexamples.get(name, 0):
            self.ownexamples[name] += 1
        else:
            self.ownexamples[name] = 1

    def __getattr__(self, name):
        if self.examples[name] != Omnibus.examples[name]:
            return Omnibus.examples[name]
        else: 
            return self.examples[name]

    def __delattr__(self, name):
        if self.ownexamples.get(name, 0):
            Omnibus.examples[name] -= 1
            del self.ownexamples[name]

import sys
exec(sys.stdin.read())
